var express = require('express');
var app = express();
var player2gameDic={};
var gameList=["Lol","RB6S"];
var gameserver2IP=[{server:"TTT",ip:"0.0.0.0"}]
app.get('/', function (req, res) {
    res.send('This is the api server');
});
app.get('/join/:uname/:game', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    player2gameDic[req.params.uname]=req.params.game;
    if(gameList.indexOf(req.params.game)>=0){
        res.send(`User ${req.params.uname} joined ${req.params.game}`);
    }
    else{
        res.send(`Failed --- User ${req.params.uname} not joined ${req.params.game}`);
    }
        
});
app.get('/leave/:uname',function(req,res){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    player2gameDic[req.params.uname]=undefined;
    res.send(`User ${req.params.uname} leaved server`);
});
app.get('/overview',function(req,res){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var o={};
    o.playergamemapping=[];
    Object.keys(player2gameDic).forEach(function(item){
        o.playergamemapping.push({"player":item,"game":player2gameDic[item]});
    });
    //o=JSON.stringify(player2gameDic);
    res.send(o);
});
app.get('/gameserver',function(req,res){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    var o={};
    o.gameserver2IP=gameserver2IP;
    //o=JSON.stringify(player2gameDic);
    res.send(o);
});
app.listen(8181, function () {
console.log('Server listening on port 8181')
});