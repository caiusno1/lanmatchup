import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PlayerGameMapping, PlayerGameWrapper } from './player-game-mapping';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PlayerGameService {
  private apiUrl = 'http://localhost:8181';
  constructor(private http: HttpClient) { }
  public getPlayerGameMappings(): Observable<PlayerGameWrapper> {
    return this.http.get<PlayerGameWrapper>(this.apiUrl + '/overview');
  }
  public switchOwnGame(name: string, game: string) {
    this.http.get(this.apiUrl + '/leave/' + name).subscribe(msg => alert(msg));
    this.http.get<PlayerGameWrapper>(this.apiUrl + '/join/' + name + '/' + game).subscribe(msg => alert(msg));
  }
}
