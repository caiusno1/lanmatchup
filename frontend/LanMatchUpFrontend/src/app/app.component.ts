import { GameserverListProviderService } from './gameserver-list-provider.service';
import { Gameserver2ipWraper } from './gameserver2ipwraper';
import { Component } from '@angular/core';
import { PlayerGameWrapper, PlayerGameMapping } from './player-game-mapping';
import { PlayerGameService } from './player-game.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'LanMatchUpFrontend';
  public playerGameMapping: PlayerGameWrapper;
  public Gameserver2ipWraper: Gameserver2ipWraper;
  constructor(private playerGameMappingService: PlayerGameService, private Gameserver2ipService: GameserverListProviderService) {
    playerGameMappingService.getPlayerGameMappings().subscribe(mapping => {
      this.playerGameMapping = mapping;
    });
    Gameserver2ipService.getServerIPMappings().subscribe(mapping => {
      this.Gameserver2ipWraper = mapping;
    });
  }
  updatePlayerGameMapping(name: string, game: string) {
    this.playerGameMappingService.switchOwnGame(name, game);
    this.playerGameMappingService.getPlayerGameMappings().subscribe(mapping => {
      this.playerGameMapping = mapping;
    });
  }
}
