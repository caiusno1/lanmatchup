import { TestBed } from '@angular/core/testing';

import { PlayerGameService } from './player-game.service';

describe('PlayerGameService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlayerGameService = TestBed.get(PlayerGameService);
    expect(service).toBeTruthy();
  });
});
