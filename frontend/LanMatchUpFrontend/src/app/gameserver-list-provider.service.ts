import { Gameserver2ipWraper } from './gameserver2ipwraper';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PlayerGameWrapper } from './player-game-mapping';

@Injectable({
  providedIn: 'root'
})
export class GameserverListProviderService {
  private apiUrl = 'http://localhost:8181';
  constructor(private http: HttpClient) { }
  public getServerIPMappings(): Observable<Gameserver2ipWraper> {
    return this.http.get<Gameserver2ipWraper>(this.apiUrl + '/gameserver');
  }
}
