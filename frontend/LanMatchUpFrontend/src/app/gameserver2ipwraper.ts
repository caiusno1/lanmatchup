export class Gameserver2ipWraper {
  public gameserver2IP: Gameserver2IPMapping;
}
export class Gameserver2IPMapping {
  public server: string;
  public ip: string;
}
