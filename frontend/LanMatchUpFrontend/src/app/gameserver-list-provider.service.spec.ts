import { TestBed } from '@angular/core/testing';

import { GameserverListProviderService } from './gameserver-list-provider.service';

describe('GameserverListProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GameserverListProviderService = TestBed.get(GameserverListProviderService);
    expect(service).toBeTruthy();
  });
});
